package com.example.recuperacionexamen;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class ConversionActivity extends AppCompatActivity {

    private EditText etCantidad;
    private RadioGroup rgConversion;
    private TextView tvResultadoConversion, tvNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion);

        etCantidad = findViewById(R.id.etCantidad);
        rgConversion = findViewById(R.id.rgConversion);
        tvResultadoConversion = findViewById(R.id.tvResultadoConversion);
        tvNombre = findViewById(R.id.tvNombre);

        String nombre = getIntent().getStringExtra("nombre");
        if (nombre != null) {
            tvNombre.setText("Bienvenido, " + nombre);
        }

        Button btnCalcularConversion = findViewById(R.id.btnCalcularConversion);
        btnCalcularConversion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarCampos()) {
                    calcularConversion();
                }
            }
        });

        Button btnLimpiar = findViewById(R.id.btnLimpiar);
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        Button btnRegresar = findViewById(R.id.btnRegresar);
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private boolean validarCampos() {
        boolean valido = true;
        if (etCantidad.getText().toString().isEmpty()) {
            etCantidad.setError("Campo requerido");
            valido = false;
        }
        if (rgConversion.getCheckedRadioButtonId() == -1) {
            Toast.makeText(this, "Seleccione una opción de conversión", Toast.LENGTH_SHORT).show();
            valido = false;
        }
        return valido;
    }

    private void calcularConversion() {
        double cantidad = Double.parseDouble(etCantidad.getText().toString());
        if (rgConversion.getCheckedRadioButtonId() == R.id.rbCelsiusAFahrenheit) {
            double resultado = (cantidad * 9/5) + 32;
            tvResultadoConversion.setText(String.format("Resultado: %.2f °F", resultado));
        } else if (rgConversion.getCheckedRadioButtonId() == R.id.rbFahrenheitACelsius) {
            double resultado = (cantidad - 32) * 5/9;
            tvResultadoConversion.setText(String.format("Resultado: %.2f °C", resultado));
        }
    }

    private void limpiarCampos() {
        etCantidad.setText("");
        rgConversion.clearCheck();
        tvResultadoConversion.setText("Resultado");
    }
}