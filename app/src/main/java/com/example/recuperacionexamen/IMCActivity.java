package com.example.recuperacionexamen;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class IMCActivity extends AppCompatActivity {

    private EditText etAlturaMetros, etAlturaCentimetros, etPeso;
    private TextView tvResultadoIMC, tvNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imcactivity);

        etAlturaMetros = findViewById(R.id.etAlturaMetros);
        etAlturaCentimetros = findViewById(R.id.etAlturaCentimetros);
        etPeso = findViewById(R.id.etPeso);
        tvResultadoIMC = findViewById(R.id.tvResultadoIMC);
        tvNombre = findViewById(R.id.tvNombre);

        String nombre = getIntent().getStringExtra("nombre");
        if (nombre != null) {
            tvNombre.setText("Bienvenido, " + nombre);
        }

        Button btnCalcularIMC = findViewById(R.id.btnCalcularIMC);
        btnCalcularIMC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarCampos()) {
                    calcularIMC();
                }
            }
        });

        Button btnLimpiar = findViewById(R.id.btnLimpiar);
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        Button btnRegresar = findViewById(R.id.btnRegresar);
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private boolean validarCampos() {
        boolean valido = true;
        if (etAlturaMetros.getText().toString().isEmpty()) {
            etAlturaMetros.setError("Campo requerido");
            valido = false;
        }
        if (etAlturaCentimetros.getText().toString().isEmpty()) {
            etAlturaCentimetros.setError("Campo requerido");
            valido = false;
        }
        if (etPeso.getText().toString().isEmpty()) {
            etPeso.setError("Campo requerido");
            valido = false;
        }
        return valido;
    }

    private void calcularIMC() {
        double alturaMetros = Double.parseDouble(etAlturaMetros.getText().toString());
        double alturaCentimetros = Double.parseDouble(etAlturaCentimetros.getText().toString());
        double alturaTotal = alturaMetros + (alturaCentimetros / 100);
        double peso = Double.parseDouble(etPeso.getText().toString());
        double imc = peso / (alturaTotal * alturaTotal);
        tvResultadoIMC.setText(String.format("Tu IMC es: %.2f kg/m²", imc));
    }

    private void limpiarCampos() {
        etAlturaMetros.setText("");
        etAlturaCentimetros.setText("");
        etPeso.setText("");
        tvResultadoIMC.setText("");
    }
}