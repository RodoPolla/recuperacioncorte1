package com.example.recuperacionexamen;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private EditText etNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etNombre = findViewById(R.id.etNombre);

        Button btnIMC = findViewById(R.id.btnIMC);
        btnIMC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarNombre()) {
                    String nombre = etNombre.getText().toString();
                    Intent intent = new Intent(MainActivity.this, IMCActivity.class);
                    intent.putExtra("nombre", nombre);
                    startActivity(intent);
                }
            }
        });

        Button btnConversor = findViewById(R.id.btnConversor);
        btnConversor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarNombre()) {
                    String nombre = etNombre.getText().toString();
                    Intent intent = new Intent(MainActivity.this, ConversionActivity.class);
                    intent.putExtra("nombre", nombre);
                    startActivity(intent);
                }
            }
        });

        Button btnSalir = findViewById(R.id.btnSalir);
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private boolean validarNombre() {
        if (etNombre.getText().toString().isEmpty()) {
            etNombre.setError("El nombre es requerido");
            return false;
        }
        return true;
    }
}